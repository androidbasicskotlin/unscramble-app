package com.example.android.unscramble.ui.game

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class GameViewModel : ViewModel() {

    //Add backing property to score
        // Add LiveData to score
    private val _score = MutableLiveData(0)
    val score: LiveData<Int>
        get() = _score
    //Add backing property to currentWordCount
        // Add LiveData to currentWordCount
    private var _currentWordCount = MutableLiveData(0)
    val currentWordCount: LiveData<Int>
        get() = _currentWordCount
    //Add backing property to currentScrambledWord
        // Add LiveData to currentScrambledWord
    private val _currentScrambledWord = MutableLiveData<String>()
    val currentScrambledWord: LiveData<String>
        get() = _currentScrambledWord

    // a MutableList<String> called wordsList to hold a list of words
    private var wordsList: MutableList<String> = mutableListOf()
    // a class variable called currentword to hold the word the player is trying to unscramble
    private lateinit var currentWord: String


    init {
        Log.d("GameFragment", "GameViewModel created!")
        getNextWord()
    }

    override fun onCleared() {
        super.onCleared()
        Log.d("GameFragment", "GameViewModel destroyed!")
    }

    private fun  getNextWord() {
        currentWord = allWordsList.random()
        val tempWord = currentWord.toCharArray()
        // To avoid currentword to be equal to the suffled Word
        while (String(tempWord).equals(currentWord, false)){
            tempWord.shuffle()
        }
        // To check if the word has already been used
        if (wordsList.contains(currentWord)){
            getNextWord()
        } else {
            _currentScrambledWord.value = String(tempWord)
            _currentWordCount.value = (_currentWordCount.value)?.inc()
            wordsList.add(currentWord)
        }

    }

    /*
    * Returns true if the current word count is less than MAX_NO_OF_WORDS.
    * Updates the next word.
    */
    fun nextWord(): Boolean {
        return if (_currentWordCount.value!! < MAX_NO_OF_WORDS){
            getNextWord()
            true
        } else false
    }

    /*
    * function to increase the score variable by SCORE_INCREASE
     */
    private fun increaseScore() {
        _score.value = (_score.value)?.plus(SCORE_INCREASE)
    }

    /*
    * Helper method returns a Boolean and takes a String (the player's word as parameter)
     */
    fun isUserWordCorrect(playerWord: String): Boolean {
        if (playerWord.equals(currentWord, true)) {
            increaseScore()
            return true
        }
        return false
    }

    fun reinitializeData() {
        _score.value = 0
        _currentWordCount.value = 0
        wordsList.clear()
        getNextWord()
    }
}